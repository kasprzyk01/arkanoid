var indexSectionsWithContent =
{
  0: "abcdegiklmnprsuvw~",
  1: "beglmpu",
  2: "u",
  3: "beglmpu",
  4: "abcdegiklmprsu~",
  5: "bcdglv",
  6: "b",
  7: "dnsw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Klasy",
  2: "Przestrzenie nazw",
  3: "Pliki",
  4: "Funkcje",
  5: "Zmienne",
  6: "Wyliczenia",
  7: "Wartości wyliczeń"
};

