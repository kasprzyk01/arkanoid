var searchData=
[
  ['game',['Game',['../class_game.html#aa43148854e7e015a6f672dd6aa00be57',1,'Game']]],
  ['gameover',['gameOver',['../class_game.html#ac7d371f3f30513a4f3c57f521fac9b5f',1,'Game']]],
  ['getbonus',['getBonus',['../class_game.html#a64a012e6b79ebdc56b479bd39a7033ff',1,'Game']]],
  ['getbonustypes',['getBonusTypes',['../class_game.html#a471c853d88d124bb091e4b24e0de6bdb',1,'Game']]],
  ['getheight',['getHeight',['../class_bonus.html#a0b1ca257a3e45fa60737c6602d12919a',1,'Bonus::getHeight()'],['../class_brick.html#a30b6331e6e959f8e1b31e8d726640243',1,'Brick::getHeight()'],['../class_platform.html#a17c2b38c702045e04b8235ed5d05f725',1,'Platform::getHeight()']]],
  ['getlife',['getLife',['../class_brick.html#aa92a991f53b5355c4efc8ddf14777043',1,'Brick']]],
  ['getmap',['getMap',['../class_main_window.html#a0bde90b5db2559a2457026179ec835c0',1,'MainWindow']]],
  ['getradius',['getRadius',['../class_ball.html#adb74fb14fde68fafbf3226c106d60da5',1,'Ball::getRadius()'],['../class_life.html#a63dad93db574512ed5fc3e9f66ee30e9',1,'Life::getRadius()'],['../class_platform.html#adb8fa00f0c3d2bd98183d94fbb68fe6e',1,'Platform::getRadius()']]],
  ['getwidth',['getWidth',['../class_bonus.html#a648ccbee0c31070c8426480aba857821',1,'Bonus::getWidth()'],['../class_brick.html#a5a0497d19b5d7d44a259367c3b2b3abf',1,'Brick::getWidth()'],['../class_platform.html#a6bcb034156fdb5bef41fbbd2c5d290d2',1,'Platform::getWidth()']]]
];
