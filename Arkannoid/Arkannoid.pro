#-------------------------------------------------
#
# Project created by QtCreator 2016-06-07T15:17:39
#
#-------------------------------------------------

QT       += core gui\
         multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Arkannoid
TEMPLATE = app
QMAKE_CXXFLAGS_WARN_ON += -Wno-reorder

SOURCES += main.cpp\
        mainwindow.cpp \
    game.cpp \
    element.cpp \
    movableelement.cpp \
    unmovableelement.cpp \
    ball.cpp \
    platform.cpp \
    bonus.cpp \
    brick.cpp \
    life.cpp

HEADERS  += mainwindow.h \
    game.h \
    element.h \
    movableelement.h \
    unmovableelement.h \
    ball.h \
    platform.h \
    bonus.h \
    brick.h \
    life.h

FORMS    += mainwindow.ui
